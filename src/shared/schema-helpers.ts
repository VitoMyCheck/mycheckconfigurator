export interface ConfigSchema {
	[x: string]: any,
}

export const cssNumberList: string[] = [
	'borderBottomWidth',
	'borderRadius',
	'fontSize',
	'fontWeight',
	'height',
	'left',
	'lineSpacing',
	'lineHeight',
	'margin',
	'marginTop',
	'padding',
	'paddingTop',
	'top',
	'bottom',
	'width',
	'zIndex',
	'right'
];

export const blockCSSPropertyList: string[] = [
	'background',
	'backgroundColor',
	'borderColor',
	'border',
	'color',
	'cursor',
	'display',
	'filter',
	'float',
	'font',
	'fontFamily',
	'overflow',
	'position',
	'textAlign',
	'textDecoration',
	'verticalAlign',
	'visibility',
];

export const inputCSSPropertyList: string[] = [
	'background',
	'backgroundColor',
	'border',
	'borderColor',
	'borderBottomWidth',
	'borderRadius',
	'color',
	'cursor',
	'display',
	'float',
	'font',
	'fontFamily',
	'fontSize',
	'fontWeight',
	'height',
	'left',
	'lineSpacing',
	'lineHeight',
	'margin',
	'marginTop',
	'overflow',
	'padding',
	'paddingTop',
	'position',
	'textAlign',
	'textDecoration',
	'top',
	'verticalAlign',
	'visibility',
	'width',
	'zIndex',
];

export const textCSSPropertyList: string[] = [
	'borderColor',
	'borderBottomWidth',
	'borderRadius',
	'color',
	'cursor',
	'display',
	'float',
	'font',
	'fontFamily',
	'fontSize',
	'fontWeight',
	'height',
	'left',
	'lineSpacing',
	'lineHeight',
	'padding',
	'position',
	'textAlign',
	'textDecoration',
	'verticalAlign',
	'width',
	'zIndex',
];

export const iconCSSPropertyList: string[] = [
	'color',
]

export const iconCSSPropertyListNum: string[] = [
	'fontSize',
]

export const componentsPositionList: string[] = [
	'paymentMethodsHeader',
	'giftcard',
	'creditCard',
	'doNotStore',
	'acceptedCards',
	'secureText',
	'addCardWith',
	'addCardWith',
	'alternatives',
];

export const settingsBoolNamesList: string[] = [
	'alternativeInsideWallet',
	'checkoutPage',
]

export const settingsComponentsBoolNamesList: string[] = [
	'paymentMethodsHeader',
	'giftCardsInfo',
	'creditCard',
	'acceptedCards',
	'secureText',
];

export const generateSettingsBoolFields = (boolNames: string[]): ConfigSchema => {
	return boolNames.reduce((acc, boolName) => {
		const newProp = {
			[boolName]: {
				type: 'boolean',
				title: boolName,
				ui: {
					widget: 'switch',
				},
				default: false
			}
		}
		return { ...acc, ...newProp };
	}, {})
}

export const generateTextFieldFrom = (propertyList: string[]): ConfigSchema => {
	return propertyList.reduce((acc, property: string): any => {
		const newProp = {
			[property]: {
				type: 'string',
				title: property,
				default: ''
			}
		}
		return { ...acc, ...newProp }
	}, {})
};

export const generateNumberFieldFrom = (propertyList: string[]): ConfigSchema => {
	return propertyList.reduce((acc, property: string): any => {
		const newProp = {
			[property]: {
				type: 'number',
				title: property,
				default: 0
			}
		}
		return { ...acc, ...newProp }
	}, {})
};

export const generateObjectSchema = (
	name: string,
	properties: any,
	title?: string,
	expandable?: boolean,
	description?: string,
): ConfigSchema => {
	if (!title) {
		return {
			[name]: {
				type: "object",
				title: name,
				properties: {...properties},
				description,
				ui: {
					expandable: expandable ? expandable : true,
				}
			}
		}
	}
	
	return {
		[name]: {
			type: "object",
			title: title,
			properties: {...properties},
			description,
			ui: {
				expandable: expandable ? expandable : true,
			}
		}
	}
}
