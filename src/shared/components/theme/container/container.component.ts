import { css, html, LitElement, TemplateResult } from 'lit-element';

class Container extends LitElement {
	static get styles() {
		return css`
			.container {
				background: rgba( 252, 252, 252, 0.30 );
				box-shadow: 0 5px 12px 0 rgba( 31, 38, 135, 0.17 );
				backdrop-filter: blur( 4.0px );
				-webkit-backdrop-filter: blur( 4.0px );
				border: 1px solid rgba( 255, 255, 255, 0.18 );
				height: var(--height);
				border-radius: var(--border-radius);
				-webkit-backface-visibility: hidden;
				-webkit-perspective: 1000;
				-webkit-transform: translate3d(0,0,0);
				-webkit-transform: translateZ(0);
				backface-visibility: hidden;
				perspective: 1000;
				transform: translate3d(0,0,0);
				transform: translateZ(0);
			}
	`
	}
	
	public render(): TemplateResult {
		return html`
			<div class="container">
				<slot></slot>
			</div>
		`
	}
}

customElements.define('glass-container', Container);
