import { css, html, TemplateResult } from 'lit-element';
import { MobxLitElement } from "@adobe/lit-mobx";
import '@polymer/paper-toolbar/paper-toolbar';
import '@polymer/paper-icon-button/paper-icon-button';
import '@polymer/iron-icons/iron-icons';

import '@polymer/paper-listbox';
import '@polymer/paper-item';
// @ts-ignore
import { IRoute, routerStore } from '@/infrastructure/routing/router.store';
import '@components/theme';

class AppDrawer extends MobxLitElement {

	static get styles() {
		return css`
			.app-drawer {
				flex: 1 1 0%;
				width: 100%;
				height: 100%;
				position: fixed;
				left: 0px;
        top: 65px;
				max-width: 300px;
				min-width: 300px;
				z-index: -1;
			}
			.logo-container {
				width: 100%;
				height: 64px;
			}
			.logo-container > img {
				width: 139px;
			}
			paper-item:hover {
				cursor: pointer;
			}
			paper-item.link.iron-selected {
				color: rgb(63, 81, 181);
				background: none;
			}
			glass-container {
				--height: 100%;
			}
      .divider {
    	  width: 95%;
        margin: 0 auto;
        height: 1px;
        background: rgba(31, 38, 135, 0.17);
    	}
		`;
	}

	private _renderLinks = () => {
		return html`
			<paper-listbox selected="0">
				${routerStore.routes.map((route: IRoute) => {
					return html`
						<paper-item
							class="link"
							@click="${() => routerStore.routeChange(route.routeName, route.path)}"
						>
							${route.routeName}
						</paper-item>
					`
				})}
			</paper-listbox>
		`
	}

	public render(): TemplateResult {
		return html`
			<div class="app-drawer">
				<glass-container>
					<div class="logo-container">
						${this._renderLinks()}
					</div>
				</glass-container>
			</div>
		`
	}
}

customElements.define('app-drawer', AppDrawer);
