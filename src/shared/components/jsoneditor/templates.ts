export const WalletV3GeneralTemplate = {
	text: 'Wallet V3 General',
	title: 'Insert Wallet V3 Config',
	className: 'v3-general',
	field: 'general',
	value: {
		"sections": {
			"background": {
				"color": "rgba(172,172,172,0.2)"
			},
			"border": {
				"color": "#ffffff",
				"width": 3,
				"radius": 0
			}
		},
		"input": {
			"height": 53,
			"border": {
				"color": "#000",
				"width": 1,
				"radius": 20,
				"shadow": true,
				"side": "all"
			},
			"background": {
				"color": "#fff"
			},
			"placeholder": {
				"display": "floating",
				"color": "#eee"
			},
			"error": {
				"border": {
					"color": "#b51d1d"
				}
			}
		},
		"checkbox": {
			"size": 18,
			"border": {
				"color": "#000",
				"width": 1,
				"radius": 4
			},
			"background": {
				"color": "#0a2e8a"
			},
			"selected": {
				"size": 16,
				"color": "#000"
			}
		},
		"radio": {
			"background": {
				"color": "#8a0a0a"
			},
			"border": {
				"color": "#000"
			},
			"selected": {
				"color": "#fbb035"
			}
		},
		"link": {
			"font": "Arial",
			"font-native": "arialBold",
			"color": "#eee",
			"text-decoration": "underline"
		},
		"dividers": {
			"color": "#ccc",
			"width": 1
		},
		"button": {
			"default": {
				"text": {
					"color": "#FBB035",
					"size": 12,
					"weight": 700
				}
			},
			"primary": {
				"background": {
					"color": "#000000"
				},
				"border": {
					"color": "#000000",
					"width": 0,
					"radius": 20
				},
				"text": {
					"color": "#fbb035",
					"font": "Arial",
					"font-native": "proximaNovaSoftRegular",
					"size": 14,
					"weight": 700
				}
			},
			"secondary": {
				"background": {
					"color": "#eeeeee"
				},
				"border": {
					"color": "#000000",
					"width": 3,
					"radius": 20
				},
				"text": {
					"color": "#000000",
					"font": "Arial",
					"font-native": "arialBold",
					"size": 14,
					"weight": 700
				}
			},
			"yes": {
				"text": {
					"color": "#b4d5c3",
					"font": "Arial",
					"font-native": "arialBold",
					"size": 12
				}
			},
			"no": {
				"text": {
					"color": "#f70a0a",
					"font": "Arial",
					"font-native": "arialBold",
					"size": 12
				}
			},
			"remove": {
				"text": {
					"color": "#db1414",
					"font": "Arial",
					"font-native": "arialBold",
					"size": 14,
					"weight": 500,
					"text-decoration": "underline"
				}
			},
			"balance": {
				"text": {
					"color": "#000",
					"font": "Arial",
					"font-native": "arialBold",
					"size": 12,
					"weight": 500
				}
			}
		},
		"tooltip": {
			"border": {
				"color": "#ffffff",
				"width": 0,
				"radius": 10
			},
			"background": {
				"color": "#0f0f0f"
			},
			"text": {
				"color": "#fff",
				"size": 12
			}
		},
		"error": {
			"text": {
				"color": "#9fe820"
			}
		},
		"icons": {
			"lock": "http://mycheckapp.com/icons/lock.svg",
			"info": "http://mycheckapp.com/icons/info.svg",
			"cc": "http://mycheckapp.com/icons/cc.svg",
			"scan-card": "http://mycheckapp.com/icons/scan-card.svg",
			"arrow-farward": "http://mycheckapp.com/icons/arrow-farward.svg",
			"arrow-backward": "http://mycheckapp.com/icons/arrow-back.svg"
		},
		"text": {
			"general": {
				"font": "Arial",
				"font-native": "arialBold"
			},
			"text1": {
				"size": 20,
				"color": "#000",
				"font": "Arial",
				"font-native": "arialBold",
				"weight": 500
			},
			"text11": {
				"size": 14,
				"color": "#000",
				"weight": 700
			},
			"text12": {
				"size": 17,
				"color": "#000"
			},
			"text2": {
				"size": 12,
				"color": "#000"
			},
			"text21": {
				"size": 13,
				"color": "#000"
			},
			"text3": {
				"size": 16,
				"color": "#000"
			},
			"text31": {
				"size": 11,
				"color": "#000"
			},
			"text32": {
				"size": 11,
				"color": "#000"
			},
			"text4": {
				"size": 10,
				"color": "#000"
			}
		},
		"rtl": {
			"supported": false
		}
	}
}
export const WalletV3SectionsTemplate = {
	text: 'Wallet V3 Sections',
	title: 'Insert Wallet V3 Config',
	className: 'v3-sections',
	field: 'sections',
	value: {
		"title": {
			"visible": false
		},
		"credit-cards": {
			"visible": true,
			"do-not-store": {
				"visible": true
			},
			"cards-accepted": {
				"visible": true,
				"list": [
					"visa",
					"mastercard",
					"amex",
					"diners"
				]
			},
			"zip-code": {
				"visible": true
			},
			"icons": {
				"cc": {
					"visible": true,
					"color": "#fff",
					"size": 12
				},
				"lock": {
					"visible": true,
					"color": "#fff",
					"size": 12
				},
				"info": {
					"visible": true,
					"color": "#fff",
					"size": 12
				},
				"scan-card": {
					"visible": true,
					"color": "#fff",
					"size": 12
				}
			}
		},
		"alternatives": {
			"enabled": {
				"masterpass": {
					"url": "https://mywalletcdn-prod.mycheckapp.com/external/masterpass.html"
				},
				"amex": {
					"client_id": "cc8eb057-7f93-41a5-8f4e-9fb51d835b78",
					"theme": "mobile",
					"button_color": "dark",
					"action": "checkout",
					"locale": "en_US",
					"country": "US",
					"callback": "aec_callback_handler",
					"env": "qa",
					"dialog_type": "popup",
					"disable_btn": "false"
				},
				"paypal": {
					"env": "sandbox",
					"billingAgreementDescription": "Tel-Aviv",
					"locale": "fr_FR"
				},
				"gpay": {
					"supportedCardTypes": [
						"visa",
						"mastercard",
						"amex"
					],
					"merchantCheckoutID": "REDLIONHTL01",
					"env": "TEST",
					"publicKey": "BNVioLxFtfRJxt09Isq/Xvc+q+GSMmxtc4fpxJWw3ExHvw3eWTlIEQpvZaQvMWwvUXUJx8WWTLqD/7FCGDp8m14=",
					"merchantInfo": {
						"merchantName": "Red Lion Hotels",
						"merchantId": "test"
					},
					"gateway": "mycheck"
				},
				"amazonpay": {
					"type": "LwA",
					"color": "Gold",
					"size": "medium",
					"merchantId": "A21GHGVQZIRSP",
					"singleCard": true
				}
			}
		},
		"instant-payments": {
			"general:": {
				"selected": {
					"shadow": {
						"color": "#fff"
					}
				}
			},
			"enabled": {
				"applepay": {
					"currencyCode": "USD",
					"merchant": "merchant.com.mycheck",
					"supportedApplePayCardTypes": [
						"visa",
						"mastercard",
						"amex"
					],
					"countryCode": "US",
					"ApplePayPreAuthTxt": "Mycheck"
				},
				"alipay": {
					"set": true
				},
				"wechat": {
					"set": true
				},
				"payconiq": {
					"set": true
				}
			},
			"icons": {
				"arrow-farward": {
					"color": "#fff",
					"size": 12
				},
				"arrow-backward": {
					"color": "#fff",
					"size": 12
				}
			}
		},
		"giftcards": {
			"visible": true,
			"balance-details-modal": {
				"background": {
					"color": "#fff"
				},
				"header": {
					"background": {
						"color": "#fff"
					}
				},
				"radius": 12
			},
			"explanatory-text": {
				"color": "#0f0f0f0"
			}
		}
	}
}
export const WalletV3LanguagesTemplate = {
	text: 'Wallet V3 Languages Section',
	title: 'Insert Wallet V3 Config',
	className: 'v3-langs',
	field: 'languages',
	value: {
		"default": "en",
		"metadata": {
			"version": 1
		},
		"translations": {
			"en": {
				"DO_NOT_STORE": "Do not Store"
			}
		}
	}
}

export const walletV3Templates = [
	WalletV3GeneralTemplate,
	WalletV3SectionsTemplate,
	WalletV3LanguagesTemplate,
]

//--------------
export const WalletV2StylesTemplate = {
	text: 'Wallet V2 Styles',
	title: 'Insert Wallet V2 Config',
	className: 'v2-styles',
	field: 'styles',
	value: {
		"container": {
			"padding": 0,
			"margin": 15
		},
		"checkout": {
			"input": {
				"default": {
					"borderColor": "#D7D7D7",
					"borderRadius": 0,
					"borderWidth": 1,
					"backgroundColor": "#FFFFFF",
					"height": 40
				},
				"error": {
					"borderColor": "#FF0404",
					"width": 0,
					"backgroundColor": "none"
				}
			},
			"text": {
				"run1": {
					"fontFamily": "Gotham-Book",
					"fontSize": 14,
					"color": "#000000"
				},
				"run2": {
					"fontFamily": "Gotham-Book",
					"fontSize": 14,
					"color": "#000000"
				},
				"link": {
					"fontFamily": "Gotham-Book",
					"fontSize": 13,
					"color": "#000000"
				},
				"error": {
					"fontFamily": "Gotham-Book",
					"fontSize": 16,
					"color": "#FF0404"
				},
				"headerTitle": {
					"fontFamily": "Gotham-Book",
					"fontSize": 15,
					"color": "#000000"
				},
				"inputTitle": {
					"fontFamily": "OpenSans-Normal",
					"fontSize": 10,
					"color": "#000000"
				},
				"inputText": {
					"fontFamily": "Gotham-Book",
					"fontSize": 10,
					"color": "#000000"
				},
				"placeholder": {
					"fontFamily": "Gotham-Book",
					"fontSize": 10,
					"color": "#8F8F8F"
				},
				"tooltip": {
					"fontFamily": "Gotham-Book",
					"fontSize": 12,
					"color": "#000000"
				}
			},
			"button": {
				"btn1": {
					"borderColor": null,
					"borderRadius": null,
					"backgroundColor": "#3B271A",
					"height": 40
				},
				"btn2": {
					"borderColor": null,
					"borderRadius": null,
					"backgroundColor": "#D8D8D8",
					"height": 40
				},
				"text1": {
					"fontFamily": "Gotham-Book",
					"fontSize": 14,
					"color": "#FFFFFF"
				},
				"text2": {
					"fontFamily": "Gotham-Book",
					"fontSize": 14,
					"color": "#FFFFFF"
				}
			},
			"icon": {
				"Privacy": {
					"color": "#000000",
					"fontSize": 30
				},
				"card": {
					"color": "#787878",
					"fontSize": 40
				}
			}
		},
		"manage": {
			"input": {
				"default": {
					"borderColor": "#D7D7D7",
					"borderRadius": 0,
					"borderWidth": 1,
					"backgroundColor": "#FFFFFF",
					"height": 40
				},
				"error": {
					"borderColor": "#FF0404",
					"Width": null,
					"backgroundColor": null
				}
			},
			"text": {
				"run1": {
					"fontFamily": "Gotham-Book",
					"fontSize": 14,
					"color": "#202020"
				},
				"run2": {
					"fontFamily": "Gotham-Book",
					"fontSize": 13,
					"color": "#979797"
				},
				"link": {
					"fontFamily": "Gotham-Book",
					"fontSize": 13,
					"color": "#979797"
				},
				"error": {
					"fontFamily": "Gotham-Book",
					"fontSize": 16,
					"color": "#D0021B"
				},
				"headerTitle": {
					"fontFamily": "OpenSans-Bold",
					"fontSize": 14,
					"color": "#FFFFFF"
				},
				"headerAction": {
					"fontFamily": "OpenSans-Bold",
					"fontSize": 12,
					"color": "#FFFFFF"
				}
			},
			"button": {
				"btn1": {
					"borderColor": "#000000",
					"borderRadius": 40,
					"backgroundColor": "#FFFFFF",
					"height": 40
				},
				"btn2": {
					"borderColor": "#000000",
					"borderRadius": 40,
					"backgroundColor": "#FFFFFF",
					"height": 40
				},
				"text1": {
					"fontFamily": "Gotham-Book",
					"fontSize": 14,
					"color": "#FFFFFF"
				}
			},
			"icon": {
				"addCard": {
					"color": "#000000",
					"fontSize": 30
				},
				"Camera": {
					"color": "#000000",
					"fontSize": 30
				},
				"header": {
					"color": "#FFFFFF",
					"fontSize": 30
				},
				"xOverlay": {
					"color": "#000000",
					"fontSize": 25
				},
				"defaultCard": {
					"color": "#C98321",
					"fontSize": 30
				},
				"storeCardCheckBox": {
					"color": "#000000",
					"fontSize": 15
				},
				"creditCard": {
					"color": "#787878",
					"fontSize": 60
				},
				"card": {
					"color": "#787878",
					"fontSize": 40
				},
				"gcCheckbox": {
					"color": "#787878",
					"fontSize": 15
				},
				"gcCheckboxActive": {
					"color": "#787878",
					"fontSize": 15
				},
				"trash": {
					"color": "#FFFFFF",
					"fontSize": 30
				}
			},
			"statusBar": {
				"default": {
					"color": "light-content",
					"backgroundColor": "none"
				},
				"overlay": {
					"color": "dark-content",
					"backgroundColor": "none"
				}
			},
			"defaultCardCircle": {
				"backgroundColor": "#FFFFFF"
			},
			"addNewCard": {
				"backgroundColor": "#FFFFFF"
			}
		},
		"background": {
			"default": {
				"backgroundColor": "#FFFFFF",
				"secondaryColor": "#FFFFFF"
			},
			"acceptedCardTitle": {
				"backgroundColor": "#0e33ed"
			},
			"overlay": {
				"backgroundColor": "#FFFFFF"
			},
			"header": {
				"backgroundColor": "#000000"
			},
			"tooltip": {
				"backgroundColor": "#FFFFFF",
				"borderColor": "#000000",
				"borderWidth": 2
			},
			"divider": {
				"borderBottomWidth": 1,
				"borderBottomColor": "#787878"
			}
		},
		"settings": {
			"placeholders": false,
			"tooltip": true,
			"acceptedCards": false,
			"checkoutButtons": false,
			"secureText": true,
			"secureTextPosition": 2,
			"paymentMethodsHeader": false,
			"titleInsideFields": false
		}
	}
}
export const WalletV2SettingsTemplate = {
	text: 'Wallet V2 Settings',
	title: 'Insert Wallet V2 Config',
	className: 'v2-settings',
	field: 'settings',
	value: {
		"local": "en_GB",
		"components": {
			"paymentMethodsHeader": false,
			"giftCardsInfo": false,
			"creditCard": true,
			"doNotStore": false,
			"acceptedCards": true,
			"secureText": true,
			"addCardWith": true,
			"alternatives": true
		},
		"componentsPosition": {
			"paymentMethodsHeader": "1",
			"giftcard": "2",
			"creditCard": "3",
			"doNotStore": "4",
			"acceptedCards": "5",
			"secureText": "6",
			"addCardWith": "7",
			"alternatives": "8"
		},
		"3dSecure": 1,
		"checkoutPage": false,
		"languages": {
			"en_US": "https://s3-eu-west-1.amazonaws.com/cdn-storage/translations/wallet/Omni/Translation.json",
			"en_GB": "https://s3-eu-west-1.amazonaws.com/cdn-storage/translations/wallet/Omni/Translation.json",
			"fr_FR": "https://translations-v1.mycheckapp.com/wallet/v1/fr.json"
		},
		"PCI": "https://payments-sandbox.mycheckapp.com",
		"alternativeWalletSettings": {
			"applePay": {
				"currencyCode": "USD",
				"merchant": "merchant.com.mycheck",
				"supportedApplePayCardTypes": [
					"visa",
					"mastercard",
					"amex"
				],
				"countryCode": "US",
				"ApplePayPreAuthTxt": "Mycheck"
			},
			"masterpass": {
				"url": "https://mywalletcdn-prod.mycheckapp.com/external/masterpass.html"
			},
			"googlePay": {
				"supportedCardTypes": [
					"visa",
					"mastercard",
					"amex"
				],
				"merchantCheckoutID": "REDLIONHTL01",
				"env": "TEST",
				"publicKey": "BNVioLxFtfRJxt09Isq/Xvc+q+GSMmxtc4fpxJWw3ExHvw3eWTlIEQpvZaQvMWwvUXUJx8WWTLqD/7FCGDp8m14=",
				"merchantId": "14178649154263234907",
				"merchantName": "Red Lion Hotels",
				"gateway": "mycheck"
			},
			"amexCheckout": {
				"client_id": "cc8eb057-7f93-41a5-8f4e-9fb51d835b78",
				"theme": "mobile",
				"button_color": "dark",
				"action": "checkout",
				"locale": "en_US",
				"country": "US",
				"callback": "aec_callback_handler",
				"env": "qa",
				"dialog_type": "popup",
				"button_type": "standard",
				"disable_btn": false
			},
			"visaCheckout": {
				"url": "https://mywalletcdn-test.mycheckapp.com/external/visacheckout.html"
			},
			"amazonPay": {
				"type": "PwA",
				"color": "LightGray",
				"size": "medium",
				"merchantId": "A21GHGVQZIRSP",
				"singleCard": true
			}
		},
		"assets": {
			"paymentMethods": {
				"addCard": {
					"visa": "https://s3-eu-west-1.amazonaws.com/cdn-storage/credit_logos/Visa.png",
					"mastercard": "https://s3-eu-west-1.amazonaws.com/cdn-storage/credit_logos/Master+Card.png",
					"amex": "https://s3-eu-west-1.amazonaws.com/cdn-storage/credit_logos/American+Express.png",
					"diners": "https://s3-eu-west-1.amazonaws.com/cdn-storage/credit_logos/Diners.png"
				},
				"manageCard": {
					"visa": "https://s3-eu-west-1.amazonaws.com/cdn-storage/cards_big/visa+card%403x.png",
					"mastercard": "https://s3-eu-west-1.amazonaws.com/cdn-storage/cards_big/mastercard%403x.png",
					"amex": "https://s3-eu-west-1.amazonaws.com/cdn-storage/cards_big/amex%403x.png",
					"diners": "https://s3-eu-west-1.amazonaws.com/cdn-storage/cards_big/diners%403x.png",
					"giftCard": "https://s3-eu-west-1.amazonaws.com/cdn-storage/cards_big/hello%403x.png"
				},
				"acceptedCard": {
					"visa": "https://s3-eu-west-1.amazonaws.com/cdn-storage/credit_logos_colored/visa.png",
					"mastercard": "https://s3-eu-west-1.amazonaws.com/cdn-storage/credit_logos_colored/mastercard.png",
					"amex": "https://s3-eu-west-1.amazonaws.com/cdn-storage/credit_logos_colored/amex.png",
					"diners": "https://s3-eu-west-1.amazonaws.com/cdn-storage/credit_logos_colored/diners.png"
				},
				"alternativePaymentMethods": {
					"payPal": "https://s3-eu-west-1.amazonaws.com/cdn-storage/whitelabel_tpl/cheesecake/paypalWallet.png",
					"applePay": "",
					"masterpass": "https://www.mastercard.com/mc_us/wallet/img/en/US/mcpp_wllt_btn_chk_317x074px.png",
					"googlePay": "",
					"visaCheckout": "https://s3-eu-west-1.amazonaws.com/cdn-storage/credits_logos/vco_button.png"
				},
				"teporaryCard": ""
			}
		},
		"iconFonts": {
			"path": [
				{
					"ttf": "https://s3-eu-west-1.amazonaws.com/cdn-storage/fonts/NOVO_icons_font/Bodeans/icomoon.ttf"
				},
				{
					"woff": "https://s3-eu-west-1.amazonaws.com/cdn-storage/fonts/NOVO_icons_font/Bodeans/icomoon.woff"
				},
				{
					"woff2": ""
				},
				{
					"svg": "https://s3-eu-west-1.amazonaws.com/cdn-storage/fonts/NOVO_icons_font/Bodeans/icomoon.svg"
				},
				{
					"eot": "https://s3-eu-west-1.amazonaws.com/cdn-storage/fonts/NOVO_icons_font/Bodeans/icomoon.eot"
				}
			],
			"relations": {
				"back": "e904",
				"trash": "e901",
				"Privacy": "e902",
				"addCard": "e903",
				"x": "e905",
				"card": "e906",
				"scanCard": "e907",
				"default": "e908",
				"forward": "e900"
			}
		},
		"features": {
			"alternativeWallet": [
				"amazonPay",
				"googlePay",
				"paypal",
				"visaCheckout",
				"amexCheckout",
				"masterpass",
				"applepay",
				"wechat",
				"alipay",
				"payconiq"
			],
			"acceptedCards": [
				"visa",
				"mastercard",
				"diners"
			],
			"doNotStore": false,
			"showGiftcard": true
		}
	}
}

export const walletV2Templates = [
	WalletV2StylesTemplate,
	WalletV2SettingsTemplate,
]

export const PayByTemplateV1 = {
	text: 'PayBy config',
	title: 'Insert PayBy config',
	className: 'payby-styles',
	field: 'settings',
	value: {
		"payment_request_type": [
			"qr_payment"
		],
		"payment_request_expiration_time": 100000,
		"payment_request_text": "The Village Hotel has shared a payment request for your reservation. \nPlease click on below link:",
		"payment_request_client_link": "https:\/\/payby-test.mycheckapp.com\/",
		"currency": "EUR",
		"PayBy": {
			"assets": {
				"logo": "https:\/\/mycheck-api-static-content.s3-eu-west-1.amazonaws.com\/Village\/village-logo-payby-55.png",
				"favicon": "https:\/\/mycheck-api-static-content.s3-eu-west-1.amazonaws.com\/Village\/favicon-32x32.png",
				"langLogo": "https:\/\/mycheck-general-settings.s3-eu-west-1.amazonaws.com\/payby\/icons\/languages\/globe.png"
			},
			"texts": {
				"run1": {
					"color": "#121212",
					"fontFamily": "VillageFlex",
					"size": "11pt"
				},
				"run2": {
					"color": "#3B271A",
					"fontFamily": "VillageFlex-Bold",
					"size": "12pt"
				},
				"run3": {
					"color": "#202020",
					"fontFamily": "VillageFlex",
					"size": "10pt"
				},
				"run4": {
					"color": "#3B271A",
					"fontFamily": "VillageFlex-Bold",
					"size": "12pt"
				}
			},
			"inputs": {
				"tip": {
					"background": "#FFFFFF",
					"borderColor": "#979797",
					"borderRadius": null,
					"borderSize": "1px",
					"borderType": "solid",
					"text": {
						"color": "#121212",
						"fontFamily": "VillageFlex",
						"size": "12pt"
					},
					"label": "tip",
					"placeholder": "Please enter tip"
				},
				"email": {
					"background": "#FFFFFF",
					"borderColor": "#979797",
					"borderRadius": null,
					"borderSize": "1px",
					"borderType": "solid",
					"text": {
						"color": "#121212",
						"fontFamily": "VillageFlex",
						"size": "12pt"
					}
				}
			},
			"links": {
				"color": "#000000",
				"fontFamily": "VillageFlex",
				"size": "11pt"
			},
			"buttons": {
				"complete": {
					"color": "#1A1A1A",
					"backgroundColor": "#FFFFFF",
					"fontFamily": "VillageFlex",
					"size": "14px",
					"hight": "15px",
					"borderColor": "#1A1A1A",
					"borderWidth": 3,
					"Radius": null,
					"display": "footer"
				}
			},
			"header": {
				"backgroundColor": "#1A1A1A"
			},
			"divider": {
				"color": "#E6E6E6",
				"width": null,
				"style": "solid"
			},
			"background": {
				"color": "#FFFFFF"
			},
			"urls": {
				"terms": "https:\/\/www.village-hotels.co.uk\/tcs\/",
				"privacy": "https:\/\/www.village-hotels.co.uk\/privacy-policy\/"
			},
			"settings": {
				"showComponents": {
					"main": {
						"hello": true,
						"reservationDetailsTitle": true,
						"reservationDetails": true,
						"subtotal": "true",
						"tip": true,
						"userEmail": true,
						"total": true,
						"paymentMethodsTitle": true,
						"paymentMethods": true,
						"tax": "true",
						"terms": true
					},
					"confirmation": {
						"thanks": true,
						"details": true,
						"subtotal": "true",
						"tip": "true",
						"tax": "true",
						"total": true
					}
				},
				"alternatives": [
					{
						"name": "paypal",
						"options": {
							"hideOn": {
								"paymentType": [
									"pay_and_store_card",
									"prepaid"
								]
							}
						}
					},
					{
						"name": "adyen",
						"options": {
							"hideOn": {
								"paymentType": []
							}
						}
					}
				],
				"dateFormat": "yyyy\/mm\/dd",
				"currency": "EUR",
				"acceptedCards": [
					"visa",
					"mastercard",
					"amex",
					"diners",
					"discover"
				]
			},
			"languages": {
				"default": "ua",
				"supports": [
					"en",
					"ua",
					"fr"
				],
				"base_url": "https:\/\/translations-v1.mycheckapp.com\/payby\/v1\/"
			},
			"translations": {
				"en": "https:\/\/translations-v1.mycheckapp.com\/payby\/v1\/en.json",
				"fr": "https:\/\/translations-v1.mycheckapp.com\/payby\/v1\/fr.json"
			}
		},
		"payment_request_contact_preferences": {
			"hotel_reservation": [
				"email",
				"sms"
			],
			"non_integrated": [
				"email",
				"sms"
			]
		},
		"payment_request_contact_sender": {
			"email": "info@village-hotels.com",
			"sms": "VillageHotel"
		},
		"payment_request_message_subject": "Your payment request for Village Hotels",
		"payment_request_qr_settings": null,
		"payment_request_html": null,
		"payment_request_html_non_integrated": null,
		"publishable_key": "pk_Nabb2QLYR2767zB3w9keroxiMu5ZL",
		"business_details": {
			"id": 1941,
			"name": "VillageWarr",
			"type": "HOTEL",
			"account_id": 1916,
			"parent_id": 1916,
			"ids": [
				{
					"id": 1253,
					"business_id": 1941,
					"external_name": "hotels-hub",
					"external_id": "9"
				},
				{
					"id": 1254,
					"business_id": 1941,
					"external_name": "mycheck",
					"external_id": "9431"
				}
			]
		}
	}
}

export const payByTemplates = [
	PayByTemplateV1,
]
