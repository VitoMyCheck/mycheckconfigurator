import { html, LitElement, TemplateResult } from 'lit-element';
import { getEditorStyle } from './jsoneditor.css';
import JSONEditor from "jsoneditor";

class JSONEditorComponent extends LitElement {
	public jsonData = "";
	public schema = "";
	public templates = [];
	public onSendJSON = (json: any) => json;
	private _editor: any;
	static get styles() {
		return getEditorStyle();
	}
	
	static get properties() {
		return {
			jsonData: {},
			onSendJSON: {},
			schema: {},
			templates: {},
		};
	}
	
	protected firstUpdated() {
		const container = this.shadowRoot!.getElementById("json-editor");
		this._editor = new JSONEditor(container!, {
			modes: ['tree', 'preview'],
			schema: this.schema.length ? JSON.parse(this.schema) : {},
			onChangeJSON: (json: any) => this.onSendJSON(json),
			templates: this.templates,
		});
		this._editor.set(JSON.parse(this.jsonData));
	}
	
	public render(): TemplateResult {
		return html`
			<div id="json-editor"></div>
		`
	}
}

customElements.define('json-editor', JSONEditorComponent);
