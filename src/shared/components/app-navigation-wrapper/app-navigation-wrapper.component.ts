import {css, html, TemplateResult} from 'lit-element';
import {classMap} from 'lit-html/directives/class-map';
import {MobxLitElement} from '@adobe/lit-mobx';
import '@polymer/paper-toolbar/paper-toolbar';
import '@polymer/paper-icon-button/paper-icon-button';
import '@polymer/iron-icons/iron-icons';

import {routerStore, RouterStore} from '@/infrastructure/routing/router.store';
import '@components/app-drawer/app-drawer.component';
import '@components/theme';
// @ts-ignore
import LogoImg from '@/assets/images/logo.png';

const MAX_WIDTH: number = 1000;
const TOP_MARGIN: number = 75;

class AppNavigationWrapper extends MobxLitElement {
	private _routerStore: RouterStore = routerStore;
	private _isDrawerOpened: boolean = true;
	
	static get styles() {
		return css`
			.main-container {
				position: relative;
				margin: 0 auto;
				margin-left: 310px;
				margin-top: 64px;
		    width: calc(100% - 320px);
        margin-top: ${TOP_MARGIN}px;
			}
			.main-container-closed {
				position: relative;
				margin: 0 auto;
				margin-top: 64px;
		    width: 80%;
        max-width: ${MAX_WIDTH}px;
        margin-top: ${TOP_MARGIN}px;
			}
			.title {
				margin-left: 0;
				display: flex;
				align-items: center;
			}
			.app-wrapper {
				display: flex;
			}
			.toolbar-wrapper {
				flex: 3;
				display: block;
				z-index: 2;
			}
			.app-drawer {
				z-index: 1;
			}
			.app-drawer-closed {
				margin-left: 0;
				display: none;
			}
			.toolbar {
				display: block;
			}
			paper-toolbar {
				--paper-toolbar-background: none;
				color: black;
			}
			.content-wrapper {
				margin-left: 193px;
			}
			.content-wrapper-closed {
				margin-left: 0
			}
			.route-name {
				display: inline-block;
				text-align: center;
			}
			.logo-container {
				display: inline-block;
				width: 139px;
				height: 64px;
			}
			.logo-container > img {
				width: 139px;
			}
			.glass-toolbar {
				position: fixed;
				width: 100%;
				z-index: 3;
			}
			.flex-container {
				flex: 1;
			}
		`;
	}
	
	private _toggleDrawer = async (): Promise<void> => {
		this._isDrawerOpened = !this._isDrawerOpened;
		await this.requestUpdate();
	}
	
	public render(): TemplateResult {
		return html`
			<div class="app-wrapper">
				<div class="toolbar-wrapper">
					<glass-container class="glass-toolbar">
						<paper-toolbar class="toolbar">
							<paper-icon-button
								slot="top"
								icon="menu"
								@click="${this._toggleDrawer}"
							></paper-icon-button>
							<div slot="top" class="title">
								<div class="logo-container flex-container">
									<img src="${LogoImg}" alt="MyCheck logo">
								</div>
								<div class="route-name flex-container">
									${this._routerStore.routeName}
								</div>
								<div class="flex-container"></div>
							</div>
						</paper-toolbar>
					</glass-container>
					<app-drawer
						class=${classMap({
							'app-drawer': true,
							'app-drawer-closed': !this._isDrawerOpened
						})}
					></app-drawer>
					<div class=${classMap({
						'main-container': true,
						'main-container-closed': !this._isDrawerOpened
					})}>
						<slot></slot>
					</div>
				</div>
			</div>
		`
	}
}

customElements.define('app-navigation-wrapper', AppNavigationWrapper);
