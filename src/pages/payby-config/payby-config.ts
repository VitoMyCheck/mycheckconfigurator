import { injector } from '@/injector';
import { PayByConfigRepository, PayByConfigResponseJSON } from './payby-config.types';

export const PAYBY_CONFIG_REPOSITORY: string = 'PAYBY_CONFIG_REPOSITORY';

export const PAYBY_CONFIG_SERVICE: string = 'PAYBY_CONFIG_SERVICE';
export class PayByConfigService {
	private _payByConfigRepo = injector.get<PayByConfigRepository>(PAYBY_CONFIG_REPOSITORY);
	
	public fetchPayByConfig(domain: string, identifier: string): Promise<PayByConfigResponseJSON> {
		return this._payByConfigRepo.fetchPayByConfig(domain, identifier);
	}
}
