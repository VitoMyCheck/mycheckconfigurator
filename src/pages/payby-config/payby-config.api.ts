import { PayByConfigRepository, PayByConfigResponse, PayByConfigResponseJSON } from './payby-config.types';
import { ApiClient } from '@/infrastructure/api-client';

export class HttpPayByConfigRepository implements PayByConfigRepository {
	async fetchPayByConfig(domain: string, identifier: string): Promise<PayByConfigResponseJSON> {
		try {
			await ApiClient<any>(`${domain}/prm/api/v1/payment-request/${identifier}`);
			const URL: string = `${domain}/prm/api/v1/payment-request/${identifier}/settings`;
			const payByConfigData: PayByConfigResponseJSON = await ApiClient<PayByConfigResponseJSON>(URL);
			delete payByConfigData.settings.payment_request_html;
			return payByConfigData;
		} catch (err) {
			throw err;
		}
	}
}
