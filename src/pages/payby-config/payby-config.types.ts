export interface PayByConfigRepository {
	fetchPayByConfig(domain: string, identifier: string): Promise<any>;
}

export interface PayByConfigResponseJSON {
	settings: PayByConfigResponse
}

export interface PayByConfigResponse {
	PayBy?: {
		[x: string]: any,
	},
	business_details?: {
		[x: string]: any,
	},
	currency?: string,
	payment_request_client_link?: string,
	payment_request_contact_preferences?: {
		[x: string]: any,
	},
	payment_request_contact_sender?: {
		[x: string]: any,
	},
	payment_request_expiration_time?: number,
	payment_request_html?: string,
	payment_request_html_non_integrated?: {
		[x: string]: any,
	},
	payment_request_message_subject?: string,
	payment_request_qr_settings?: {
		[x: string]: any,
	},
	payment_request_text?: string,
	payment_request_type?: string[],
	publishable_key?: string,
}

export type PayByEnvironmentNames = 'test' | 'sandbox' | 'production';
export type PayByEnvironmentsURLs = 'https://the-test.mycheckapp.com'
	| 'https://the-sandbox.mycheckapp.com'
	| 'https://the.mycheckapp.com';

export  interface PayByEnvData {
	apiDomain: PayByEnvironmentsURLs,
	env: PayByEnvironmentNames,
	identifier: string,
}

export interface PayByEnvironmentListItem {
	env: PayByEnvironmentNames,
	apiDomain: PayByEnvironmentsURLs,
}
