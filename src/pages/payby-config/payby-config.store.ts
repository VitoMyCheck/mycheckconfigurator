import { observable, action, makeObservable, computed } from 'mobx';
import { injector } from '@/injector';
import {
	PAYBY_CONFIG_SERVICE,
	PayByConfigService,
} from '@/pages/payby-config/payby-config';
import {
	PayByConfigResponse,
	PayByEnvData,
	PayByEnvironmentListItem,
} from './payby-config.types';

const DEFAULT_ENV: PayByEnvironmentListItem = {
	apiDomain: 'https://the-test.mycheckapp.com',
	env: 'test',
}

class PayByConfigStore {
	private _payByService: PayByConfigService = injector.get<PayByConfigService>(PAYBY_CONFIG_SERVICE);
	@observable
	private _payByConfig: PayByConfigResponse | undefined;
	@observable
	public editedPayByConfig: PayByConfigResponse | undefined;
	@observable
	public envData: PayByEnvData = {
		apiDomain: DEFAULT_ENV.apiDomain,
		identifier: '',
		env: DEFAULT_ENV.env,
	}
	constructor() {
		makeObservable<PayByConfigStore>(this);
	}
	
	@computed
	public get PayByConfig() {
		if (!this._payByConfig) {
			return {};
		}
		
		return this._payByConfig;
	}
	
	public set PayByConfig(config: PayByConfigResponse | undefined) {
		this._payByConfig = {...config};
	}
	
	@action
	public fetchPayByConfig = async (apiDomain: string, identifier: string): Promise<void> => {
		try {
			this.PayByConfig = await this._payByService.fetchPayByConfig(apiDomain, identifier);
			this.editedPayByConfig = {...this.PayByConfig};
		} catch (err) {
			console.log(err);
			this._payByConfig = undefined;
		}
	}
	
	@action
	public setEditedPayByConfig = (isValid: boolean, editedConfig: PayByConfigResponse): void => {
		this.editedPayByConfig = {...editedConfig};
		// TODO: Validation is more harmful for now
	}
	
	@action
	public resetEditedPayByConfig = (): void => {
		this.editedPayByConfig = this.PayByConfig;
	}
	
	@action
	public updateEnvData = (fieldName: string, value: string): void => {
		this.envData = { ...this.envData, [fieldName]: value };
	}
}

export const payByConfigStore: PayByConfigStore = new PayByConfigStore();
