import { css, html, TemplateResult } from 'lit-element';
import { MobxLitElement } from '@adobe/lit-mobx';
import { action, observable } from 'mobx';
import '@polymer/paper-input/paper-input';
import '@polymer/paper-button/paper-button';
import '@polymer/paper-card/paper-card';
import '@polymer/paper-radio-group';
import '@polymer/paper-radio-button';
import '@polymer/paper-tabs/paper-tabs';
import '@polymer/paper-tabs/paper-tab';

import { DOMEvent } from 'HTMLEvent'
import { payByConfigStore } from '@/pages/payby-config/payby-config.store';
import { PayByEnvironmentListItem } from '@/pages/payby-config/payby-config.types';

const IDENTIFIER_MAX_LENGTH: number = 36;

const ENVIRONMENTS: PayByEnvironmentListItem[] = [
	{
		apiDomain: 'https://the-test.mycheckapp.com',
		env: 'test',
	},
	{
		apiDomain: 'https://the-sandbox.mycheckapp.com',
		env: 'sandbox',
	},
	{
		apiDomain: 'https://the.mycheckapp.com',
		env: 'production',
	},
]

class PayByEnvConfiguration extends MobxLitElement {
	@observable
	private _isTemplateLoading: boolean = false;
	
	static get styles() {
		return css`
			.paper-card {
				width: 100%;
				background: none;
				box-shadow: none;
			}
			.container {
				margin-bottom: 20px;
			}
			.env-picker-container {
				display: flex;
				flex-direction: column;
			}
			.env-picker, pkey-picker {
				flex: 1
			}
			.env-radio-button {
				text-transform: capitalize;
			}
		  glass-container {
    	  --border-radius: 5px;
    	}
		`;
	}
	
	@action
	public getPayByConfigg = async (): Promise<void> => {
		const {apiDomain, identifier} = payByConfigStore.envData;
		await payByConfigStore.fetchPayByConfig(apiDomain, identifier);
		this._isTemplateLoading = true;
		await this.requestUpdate();
		this._isTemplateLoading = false;
		await this.requestUpdate();
	}
	
	@action
	private _onEnvChanged = (ev: DOMEvent<HTMLInputElement>): void => {
		const {target: {value}} = ev;
		const envData: PayByEnvironmentListItem | undefined = ENVIRONMENTS.find(envData => envData.env === value);
		payByConfigStore.updateEnvData('env', value);
		payByConfigStore.updateEnvData('apiDomain', envData!.apiDomain);
	}
	
	@action
	private _onIdentifierChange = (ev: DOMEvent<HTMLInputElement>): void => {
		const {target: {value}} = ev;
		payByConfigStore.updateEnvData('identifier', value);
	}
	
	render(): TemplateResult {
		return html`
			<glass-container>
				<paper-card class="paper-card" heading='Environment setup'>
					<div class="card-content">
						<div class="env-picker-container">
							<div class="env-picker">
								<paper-radio-group @change="${this._onEnvChanged}" selected="${payByConfigStore.envData.env}">
									${ENVIRONMENTS.map((envData: PayByEnvironmentListItem) => html`
										<paper-radio-button
											class="env-radio-button"
											value="${envData.env}"
											name="${envData.env}"
										>
											${envData.env}
										</paper-radio-button>
									`)}
								</paper-radio-group>
							</div>
						</div>
						<div class="pkey-picker">
							<paper-input
								value="${payByConfigStore.envData.identifier}"
	              @change="${this._onIdentifierChange}"
							  maxlength=${IDENTIFIER_MAX_LENGTH}
							  label="Identifier">
							</paper-inputvalue>
						</div>
					</div>
					<div class="card-actions">
						<paper-button @click="${this.getPayByConfigg}">Get</paper-button>
					</div>
				</paper-card>
			</glass-container>
		`
	}
}

customElements.define('payby-env-configuration', PayByEnvConfiguration);
