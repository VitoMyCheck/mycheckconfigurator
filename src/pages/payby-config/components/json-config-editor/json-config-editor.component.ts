import { ipcRenderer } from 'electron';
import { css, html, TemplateResult } from 'lit-element';
import { MobxLitElement } from '@adobe/lit-mobx';
import { action, observable } from 'mobx';
import '@polymer/paper-input/paper-input';
import '@polymer/paper-button/paper-button';
import '@polymer/paper-card/paper-card';
import '@polymer/paper-radio-group';
import '@polymer/paper-radio-button';
import '@polymer/paper-listbox';
import '@polymer/paper-item';
import '@components';
import 'json-form-custom-element';
import { PayByConfigSchema } from './config-schema';
import { payByTemplates } from '@/shared/components/jsoneditor/templates';

import { payByConfigStore } from "@/pages/payby-config/payby-config.store";

class JsonPayByConfigEditorComponent extends MobxLitElement {
	@observable
	public isFormValid: boolean = false;
	@observable
	public errorMessages: any;
	
	@observable
	private _isTemplateLoading: boolean = false;
	
	static get styles() {
		return css`
			.wrapper {
				display: block;
				position: relative;
			}
			.dialog {
		    margin-left: 300px;
		    width: calc(100%-300);
			}
			.paper-card {
				width: 100%;
				background: none;
				box-shadow: none;
			}
			.error-item-header {
				padding: 10px;
				box-shadow: 0 6px 9px -3px rgba(0,0,0, 0.3);
			}
			.error-item-data {
				padding: 10px;
        margin-bottom: 5px;
        border-bottom: solid 1px rgba(0,0,0,0.1);
			}
		  glass-container {
    	  --border-radius: 5px;
    	}
		`;
	}
	
	@action
	public setupEditor = (): TemplateResult => {
		const schema = JSON.stringify(PayByConfigSchema);
		return html`
			<json-editor
				.onSendJSON=${this.onFormChange}
				schema="${schema}"
				jsonData="${JSON.stringify(payByConfigStore.PayByConfig)}"
				.templates=${payByTemplates}
			></json-editor>
		`
	}
	
	@action
	public onFormChange = async (data: any): Promise<void> => {
		payByConfigStore.setEditedPayByConfig(true, data);
		await this.requestUpdate();
	}
	
	@action
	public _resetForm = async (): Promise<void> => {
		payByConfigStore.resetEditedPayByConfig();
		this._isTemplateLoading = true;
		await this.requestUpdate();
		this._isTemplateLoading = false;
		await this.requestUpdate();
	}
	
	private _saveConfig = (): void => {
		// TODO: Validation is more harmful for now
		ipcRenderer.send(
			'config',
			JSON.stringify(payByConfigStore.editedPayByConfig, null, '\t')
		);
	}
	
	private _sendToPayBy = (): void => {
		// TODO: Validation is more harmful for now
		ipcRenderer.send('payByConfiguratorChannel', JSON.stringify(payByConfigStore.editedPayByConfig));
	}
	
	private _onOpenPayByWindow = (): void => {
		ipcRenderer.send(
			'openPayByWindow',
			{
				identifier: payByConfigStore.envData.identifier,
			}
		);
	}
	
	render(): TemplateResult {
		return html`
			<div class="wrapper">
				<glass-container>
					<paper-card class="paper-card" heading='Config Editor'>
					<div id="json-form-container" class="card-content">
						${this.setupEditor()}
					</div>
					<div class="card-actions">
						<paper-button @click="${this._resetForm}">Reset Config</paper-button>
<!--						<paper-button @click="${this._onOpenPayByWindow}">Open PayBy</paper-button>-->
						<paper-button @click="${this._saveConfig}">Save to File</paper-button>
<!--						<paper-button @click="${this._sendToPayBy}">Send to PayBy</paper-button>-->
					</div>
				</paper-card>
				</glass-container>
			</div>
		`
	}
}

customElements.define('json-payby-config-editor', JsonPayByConfigEditorComponent);
