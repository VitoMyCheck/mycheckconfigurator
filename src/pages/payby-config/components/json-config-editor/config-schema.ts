import {
	ConfigSchema,
	generateNumberFieldFrom,
	generateObjectSchema,
	generateSettingsBoolFields,
	generateTextFieldFrom,
} from '@/shared/schema-helpers';

const generatePayByPart = (): ConfigSchema => ({
	PayBy: {
		type: "object",
		title: "PaBy",
		properties: {
			...generateObjectSchema(
				"assets",
				{
					...generateTextFieldFrom([
						'logo',
						'favicon',
						'langLogo',
					]),
				}
			),
			...generateObjectSchema(
				"texts",
				{
					...generateObjectSchema(
						"run1",
						{
							...generateTextFieldFrom([
								'color',
								'fontFamily',
								'size',
							]),
						}
					),
					...generateObjectSchema(
						"run2",
						{
							...generateTextFieldFrom([
								'color',
								'fontFamily',
								'size',
							]),
						}
					),
					...generateObjectSchema(
						"run3",
						{
							...generateTextFieldFrom([
								'color',
								'fontFamily',
								'size',
							]),
						}
					),
					...generateObjectSchema(
						"run4",
						{
							...generateTextFieldFrom([
								'color',
								'fontFamily',
								'size',
							]),
						}
					),
				}
			),
			...generateObjectSchema(
				"inputs",
				{
					...generateObjectSchema(
						"tip",
						{
							...generateTextFieldFrom([
								'background',
								'borderColor',
								'borderRadius',
								'borderSize',
								'borderType',
								'label',
								'placeholder',
							]),
							...generateObjectSchema(
								"text",
								{
									...generateTextFieldFrom([
										'color',
										'fontFamily',
										'size',
									]),
								}
							),
						}
					),
					...generateObjectSchema(
						"email",
						{
							...generateTextFieldFrom([
								'background',
								'borderColor',
								'borderRadius',
								'borderSize',
								'borderType',
								'label',
								'placeholder',
							]),
							...generateObjectSchema(
								"text",
								{
									...generateTextFieldFrom([
										'color',
										'fontFamily',
										'size',
									]),
								}
							),
						}
					),
				}
			),
			...generateObjectSchema(
				"links",
				{
					...generateTextFieldFrom([
						'color',
						'fontFamily',
						'size',
					]),
				}
			),
			...generateObjectSchema(
				"buttons",
				{
					...generateObjectSchema(
						"complete",
						{
							...generateTextFieldFrom([
								'color',
								'backgroundColor',
								'fontFamily',
								'size',
								'height',
								'borderColor',
								'borderWidth',
								'Radius',
								'display',
							]),
						}
					),
				}
			),
			...generateObjectSchema(
				"header",
				{
					...generateTextFieldFrom([
						'backgroundColor',
					]),
				}
			),
			...generateObjectSchema(
				"divider",
				{
					...generateTextFieldFrom([
						'color',
						'width',
						'style',
					]),
				}
			),
			...generateObjectSchema(
				"background",
				{
					...generateTextFieldFrom([
						'color',
					]),
				}
			),
			...generateObjectSchema(
				"urls",
				{
					...generateTextFieldFrom([
						'terms',
						'privacy',
					]),
				}
			),
			...generateObjectSchema(
				"settings",
				{
					...generateObjectSchema(
						"showComponents",
						{
							...generateObjectSchema(
								"main",
								{
									...generateSettingsBoolFields([
										'hello',
										'reservationDetailsTitle',
										'reservationDetails',
										'subtotal',
										'tip',
										'userEmail',
										'total',
										'paymentMethodsTitle',
										'paymentMethods',
										'tax',
										'terms',
									]),
								}
							),
							...generateObjectSchema(
								"confirmation",
								{
									...generateSettingsBoolFields([
										'thanks',
										'details',
										'subtotal',
										'tip',
										'tax',
										'total',
									]),
								}
							),
						}
					),
					alternatives: {
						title: 'Alternative Payments',
						type: 'array',
						items: {
							title: 'Alternative Payments',
							items: {
								type: 'object',
								properties: {
									...generateTextFieldFrom([
										'name',
									]),
									...generateObjectSchema(
										"options",
										{
											...generateObjectSchema(
												"hideOn",
												{
													paymentType: {
														title: 'Payment Type',
														type: 'array',
														items: {
															title: 'Payment Type',
															items: {
																type: 'string'
															}
														}
													},
												}
											),
										}
									),
								}
							}
						}
					},
					...generateTextFieldFrom([
						'dateFormat',
						'currency',
					]),
					acceptedCards: {
						title: 'Accepted Cards',
						type: 'array',
						items: {
							title: 'Accepted Cards',
							items: {
								type: 'string'
							}
						}
					},
				}
			),
			...generateObjectSchema(
				"languages",
				{
					...generateTextFieldFrom([
						'default',
						'base_url',
					]),
					supports: {
						title: 'Payment Request Types',
						type: 'array',
						items: {
							title: 'Payment Request Types',
							items: {
								type: 'string'
							}
						}
					},
				}
			),
			...generateObjectSchema(
				"translations",
				{
					...generateTextFieldFrom([
						'en',
						'fr',
					]),
				}
			),
		}
	}
});

export const PayByConfigSchema: ConfigSchema = {
	type: "object",
	properties: {
		...generateObjectSchema(
			"settings",
			{
				...generatePayByPart(),
				...generateNumberFieldFrom([
					'payment_request_expiration_time'
				]),
				...generateTextFieldFrom([
					'payment_request_text',
					'payment_request_client_link',
					'currency',
					'payment_request_message_subject',
					'publishable_key',
				]),
				payment_request_type: {
					title: 'Payment Request Types',
					type: 'array',
					items: {
						title: 'Payment Request Types',
						items: {
							type: 'string'
						}
					}
				},
				...generateObjectSchema(
					"payment_request_contact_preferences",
					{
						hotel_reservation: {
							title: 'Hotel Reservations',
							type: 'array',
							items: {
								title: 'Hotel Reservations',
								items: {
									type: 'string'
								}
							}
						},
						non_integrated: {
							title: 'Non Integrated List',
							type: 'array',
							items: {
								title: 'Non Integrated',
								items: {
									type: 'string'
								}
							}
						},
					}
				),
				...generateObjectSchema(
					"payment_request_contact_sender",
					{
						...generateTextFieldFrom([
							'email',
							'sms',
						]),
					}
				),
				...generateObjectSchema(
					"business_details",
					{
						...generateTextFieldFrom([
							'name',
							'type',
						]),
						...generateNumberFieldFrom([
							'id',
							'account_id',
							'parent_id',
						]),
						ids: {
							title: 'IDs',
							type: 'array',
							items: {
								title: 'Items',
								items: {
									type: 'object',
									properties: {
										...generateTextFieldFrom([
											'external_name',
											'external_id',
										]),
										...generateNumberFieldFrom([
											'id',
											'business_id',
										]),
									}
								}
							}
						},
					}
				),
			}
		)
	}
}
