import { LitElement, html } from 'lit-element';

class NotFoundPage extends LitElement {
	render() {
		return html`
			<div>404</div>
		`
	}
}

customElements.define('not-found-page', NotFoundPage);