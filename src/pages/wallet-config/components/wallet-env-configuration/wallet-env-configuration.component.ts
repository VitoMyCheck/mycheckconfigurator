import {css, html, TemplateResult} from 'lit-element';
import {MobxLitElement} from '@adobe/lit-mobx';
import {action, observable} from 'mobx';
import '@polymer/paper-input/paper-input';
import '@polymer/paper-button/paper-button';
import '@polymer/paper-card/paper-card';
import '@polymer/paper-radio-group';
import '@polymer/paper-radio-button';
import '@polymer/paper-tabs/paper-tabs';
import '@polymer/paper-tabs/paper-tab';

import {DOMEvent} from 'HTMLEvent'
import {walletConfigStore} from '@/pages/wallet-config/wallet-config.store';
import {WalletEnvironmentListItem, WalletVersion} from '@/pages/wallet-config/wallet-config.types';

const PKEY_MAX_LENGTH: number = 32;

const ENVIRONMENTS: WalletEnvironmentListItem[] = [
	{
		apiDomain: 'https://the-test.mycheckapp.com',
		env: 'test',
	},
	{
		apiDomain: 'https://the-sandbox.mycheckapp.com',
		env: 'sandbox',
	},
	{
		apiDomain: 'https://the.mycheckapp.com',
		env: 'production',
	},
]

const WALLET_VERSIONS: WalletVersion[] = [
	'v2',
	'v3',
]

class WalletEnvConfiguration extends MobxLitElement {
	@observable
	private _isTemplateLoading: boolean = false;
	
	static get styles() {
		return css`
			.paper-card {
				width: 100%;
				background: none;
				box-shadow: none;
			}
			.container {
				margin-bottom: 20px;
			}
			.env-picker-container {
				display: flex;
				flex-direction: column;
			}
			.env-picker, pkey-picker {
				flex: 1
			}
			.env-radio-button {
				text-transform: capitalize;
			}
		  glass-container {
    	  --border-radius: 5px;
    	}
		`;
	}
	
	@action
	public getWalletConfig = async (): Promise<void> => {
		const {apiDomain, pkey, walletVersion} = walletConfigStore.envData;
		await walletConfigStore.fetchWalletConfig(apiDomain, pkey, walletVersion);
		this._isTemplateLoading = true;
		await this.requestUpdate();
		this._isTemplateLoading = false;
		await this.requestUpdate();
	}
	
	@action
	private _onEnvChanged = (ev: DOMEvent<HTMLInputElement>): void => {
		const {target: {value}} = ev;
		const envData: WalletEnvironmentListItem | undefined = ENVIRONMENTS.find(envData => envData.env === value);
		walletConfigStore.updateEnvData('env', value);
		walletConfigStore.updateEnvData('apiDomain', envData!.apiDomain);
	}
	
	@action
	private _onWalletVersionChanged = (ev: DOMEvent<HTMLInputElement>): void => {
		const {target: {value}} = ev;
		walletConfigStore.updateEnvData('walletVersion', value);
	}
	
	@action
	private _onPkeyChange = (ev: DOMEvent<HTMLInputElement>): void => {
		const {target: {value}} = ev;
		walletConfigStore.updateEnvData('pkey', value);
	}
	
	render(): TemplateResult {
		return html`
			<glass-container>
				<paper-card class="paper-card" heading='Environment setup'>
					<div class="card-content">
						<div class="env-picker-container">
							<div class="env-picker">
								<paper-radio-group @change="${this._onWalletVersionChanged}"
								                   selected="${walletConfigStore.envData.walletVersion}">
									${WALLET_VERSIONS.map((walletVersion: WalletVersion) => html`
										<paper-radio-button
											class="env-radio-button"
											value="${walletVersion}"
											name="${walletVersion}"
										>
											${walletVersion}
										</paper-radio-button>
									`)}
								</paper-radio-group>
							</div>
							<div class="env-picker">
								<paper-radio-group @change="${this._onEnvChanged}" selected="${walletConfigStore.envData.env}">
									${ENVIRONMENTS.map((envData: WalletEnvironmentListItem) => html`
										<paper-radio-button
											class="env-radio-button"
											value="${envData.env}"
											name="${envData.env}"
										>
											${envData.env}
										</paper-radio-button>
									`)}
								</paper-radio-group>
							</div>
						</div>
						<div class="pkey-picker">
							<paper-input
								value="${walletConfigStore.envData.pkey}"
								@change="${this._onPkeyChange}"
								maxlength=${PKEY_MAX_LENGTH}
		            label="Pkey">
							</paper-inputvalue>
						</div>
					</div>
					<div class="card-actions">
						<paper-button @click="${this.getWalletConfig}">Get</paper-button>
					</div>
				</paper-card>
			</glass-container>
		`
	}
}

customElements.define('wallet-env-configuration', WalletEnvConfiguration);
