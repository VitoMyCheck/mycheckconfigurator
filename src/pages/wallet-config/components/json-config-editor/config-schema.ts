//TODO: Write schema generators for addable items. Also reduce hardcode in main schema object

import {
	blockCSSPropertyList,
	componentsPositionList,
	ConfigSchema,
	cssNumberList,
	generateNumberFieldFrom,
	generateObjectSchema,
	generateSettingsBoolFields,
	generateTextFieldFrom,
	iconCSSPropertyList,
	iconCSSPropertyListNum,
	inputCSSPropertyList,
	settingsBoolNamesList,
	settingsComponentsBoolNamesList,
	textCSSPropertyList,
} from '@/shared/schema-helpers';

const generateStyleSchemaPart = (): ConfigSchema => ({
	style: {
		type: "object",
		ui: {
			expandable: true,
		},
		title: "Style",
		properties: {
			container: {
				type: "object",
				title: "Container",
				ui: {
					expandable: true,
				},
				properties: {
					...generateTextFieldFrom(blockCSSPropertyList),
					...generateNumberFieldFrom(cssNumberList),
				}
			},
			checkout: {
				type: "object",
				title: "Checkout",
				ui: {
					expandable: true,
				},
				properties: {
					...generateObjectSchema(
						'input',
						{
							...generateObjectSchema(
								'default',
								{
									...generateTextFieldFrom(inputCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Default',
							),
							...generateObjectSchema(
								'error',
								{
									...generateTextFieldFrom(inputCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Error',
							)
						},
						'Input',
					),
					...generateObjectSchema(
						'text',
						{
							...generateObjectSchema(
								'run1',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Run1',
							),
							...generateObjectSchema(
								'run2',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Run2',
							),
							...generateObjectSchema(
								'run3',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Run3',
							),
							...generateObjectSchema(
								'run4',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Run4',
							),
							...generateObjectSchema(
								'run5',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
							),
							...generateObjectSchema(
								'run6',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
							),
							...generateObjectSchema(
								'inputText',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Input Text',
							),
							...generateObjectSchema(
								'placeholder',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Placeholder',
							),
							...generateObjectSchema(
								'tooltip',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Tooltip',
							),
						},
						'Text',
					),
					...generateObjectSchema(
						'button',
						{
							...generateObjectSchema(
								'radio',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Radio Button',
							),
							...generateObjectSchema(
								'radioSelected',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Radio Button: Selected',
							),
						},
						'Button',
					),
					...generateObjectSchema(
						'icon',
						{
							...generateObjectSchema(
								'privacy',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Privacy',
							),
						},
						'Icon',
					),
				}
			},
			manage: {
				type: "object",
				title: "Manage",
				ui: {
					expandable: true,
				},
				properties: {
					...generateObjectSchema(
						'input',
						{
							...generateObjectSchema(
								'default',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Default',
							),
							...generateObjectSchema(
								'error',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Error',
							),
							...generateObjectSchema(
								'inputText',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Input Text',
							),
						},
						'Input',
					),
					...generateObjectSchema(
						'text',
						{
							...generateObjectSchema(
								'run1',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Run 1',
							),
							...generateObjectSchema(
								'run2',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Run 2',
							),
							...generateObjectSchema(
								'text1',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Text 1',
							),
							...generateObjectSchema(
								'text2',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Text 2',
							),
							...generateObjectSchema(
								'error',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Error',
							),
						},
						'Text',
					),
					...generateObjectSchema(
						'button',
						{
							...generateObjectSchema(
								'btn1',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Wallet Redirect Accept button',
							),
							...generateObjectSchema(
								'btn2',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Wallet Redirect Decline button',
							),
							...generateObjectSchema(
								'gcCheckbox',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Gif Card Checkbox',
							),
							...generateObjectSchema(
								'swipe',
								{
									...generateTextFieldFrom(textCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Swipe Button',
							),
						},
						'Button',
					),
					...generateObjectSchema(
						'icon',
						{
							...generateObjectSchema(
								'card',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Card',
							),
							...generateObjectSchema(
								'i',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Info icon',
							),
							...generateObjectSchema(
								'Camera',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Camera',
							),
							...generateObjectSchema(
								'header',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Header',
							),
							...generateObjectSchema(
								'xOverlay',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'X button overlay',
							),
							...generateObjectSchema(
								'defaultCard',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Default Card',
							),
							...generateObjectSchema(
								'storeCardCheckBox',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Store Card CheckBox',
							),
							...generateObjectSchema(
								'creditCard',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Credit Card',
							),
							...generateObjectSchema(
								'checkboxSelect',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Checkbox Select',
							),
							...generateObjectSchema(
								'trash',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Trash',
							),
							...generateObjectSchema(
								'addCard',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Add Card',
							),
							...generateObjectSchema(
								'dropDown',
								{
									...generateTextFieldFrom(iconCSSPropertyList),
									...generateNumberFieldFrom(iconCSSPropertyListNum),
								},
								'Dropdown',
							),
						},
						'Icon',
					),
					...generateObjectSchema(
						'statusBar',
						{
							...generateObjectSchema(
								'default',
								{
									...generateTextFieldFrom(blockCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Default',
							),
							...generateObjectSchema(
								'overlay',
								{
									...generateTextFieldFrom(blockCSSPropertyList),
									...generateNumberFieldFrom(cssNumberList),
								},
								'Overlay',
							),
						},
						'Status Bar',
					),
					...generateObjectSchema(
						'defaultCardCircle',
						{
							...generateTextFieldFrom(blockCSSPropertyList),
							...generateNumberFieldFrom(cssNumberList),
						},
						'Default Card Circle (Manage Cards)',
					),
					...generateObjectSchema(
						'addNewCard',
						{
							...generateTextFieldFrom(blockCSSPropertyList),
							...generateNumberFieldFrom(cssNumberList),
						},
						'Add New Card Button',
					),
				}
			},
			background: {
				type: "object",
				title: "Background",
				ui: {
					expandable: true,
				},
				properties: {
					...generateObjectSchema(
						'default',
						{
							...generateTextFieldFrom(blockCSSPropertyList),
							...generateNumberFieldFrom(cssNumberList),
						},
						'Default',
					),
					...generateObjectSchema(
						'acceptedCardTitle',
						{
							...generateTextFieldFrom(textCSSPropertyList),
							...generateNumberFieldFrom(cssNumberList),
						},
						'Accepted Card Title',
					),
					...generateObjectSchema(
						'box',
						{
							...generateTextFieldFrom(blockCSSPropertyList),
							...generateNumberFieldFrom(cssNumberList),
						},
						'Box',
					),
					...generateObjectSchema(
						'overlay',
						{
							...generateTextFieldFrom(blockCSSPropertyList),
							...generateNumberFieldFrom(cssNumberList),
						},
						'Overlay',
					),
					...generateObjectSchema(
						'header',
						{
							...generateTextFieldFrom(blockCSSPropertyList),
							...generateNumberFieldFrom(cssNumberList),
						},
						'Header',
					),
					...generateObjectSchema(
						'tooltip',
						{
							...generateTextFieldFrom(blockCSSPropertyList),
							...generateNumberFieldFrom(cssNumberList),
						},
						'Tooltip',
					),
					...generateObjectSchema(
						'divider',
						{
							...generateTextFieldFrom(blockCSSPropertyList),
							...generateNumberFieldFrom(cssNumberList),
						},
						'Divider',
					),
					...generateObjectSchema(
						'popup',
						{
							...generateTextFieldFrom(blockCSSPropertyList),
							...generateNumberFieldFrom(cssNumberList),
						},
						'Popup',
					),
				}
			},
			settings: {
				type: "object",
				title: "Settings",
				ui: {
					expandable: true,
				},
				properties: {
					placeholders: {
						type: "boolean",
						title: 'Placeholder',
					},
					hint: {
						type: "boolean",
						title: 'Hint',
					},
					tooltip: {
						type: "boolean",
						title: 'Tooltip',
					},
					checkoutButtons: {
						type: "boolean",
						title: 'Checkout Buttons',
					},
					secureTextPosition: {
						type: "number",
						title: 'Secure Text Position',
						minimum: 1,
						maximum: 2,
					},
					titleInsideFields: {
						type: "boolean",
						title: 'Title Inside Fields',
					},
					quickBookingHeader: {
						type: "boolean",
						title: 'QuickBookingHeader',
					},
					mouseHover: {
						type: "boolean",
						title: 'Mouse Hover',
					}
				}
			},
		}
	}
})
const generateSettingsSchemaPart = (): ConfigSchema => ({
	settings: {
		type: "object",
		ui: {
			expandable: true,
		},
		title: "Settings",
		properties: {
			...generateSettingsBoolFields(settingsBoolNamesList),
			...generateTextFieldFrom(['currencyCode']),
			...generateObjectSchema(
				'components',
				{
					...generateSettingsBoolFields(settingsComponentsBoolNamesList),
				},
				'Components',
			),
			...generateObjectSchema(
				'componentsPosition',
				{
					...generateTextFieldFrom(componentsPositionList)
				},
				'Components Position'
			),
			...generateTextFieldFrom(['local', 'PCI']),
			languages: {
				title: 'Languages'
			},
			...generateObjectSchema(
				'alternativeWalletSettings',
				{
					...generateObjectSchema(
						'applePay',
						{
							...generateTextFieldFrom([
								'currencyCode',
								'merchant',
								'countryCode',
								'ApplePayPreAuthTxt',
							]),
							supportedApplePayCardTypes: {
								title: 'Supported Apple Pay Card Types',
								type: 'array',
								items: {
									title: 'Supported Apple Pay Card Type',
									items: {
										type: 'string'
									}
								}
							}
						},
						'ApplePay'
					),
					...generateObjectSchema(
						'masterpass',
						{
							...generateTextFieldFrom(['url'])
						},
						'MasterPass'
					),
					...generateObjectSchema(
						'visaCheckout',
						{
							...generateTextFieldFrom(['url'])
						},
						'Visa Checkout'
					),
					...generateObjectSchema(
						'payconiq',
						{
							...generateSettingsBoolFields(['instant']),
							...generateTextFieldFrom(['merchant_id']),
						},
						'Payconiq'
					),
					...generateObjectSchema(
						'amexCheckout',
						{
							...generateTextFieldFrom([
								'client_id',
								'theme',
								'button_color',
								'action',
								'locale',
								'country',
								'callback',
								'env',
								'dialog_type',
								'button_type',
							]),
							...generateSettingsBoolFields([
								'disable_btn'
							])
						},
						'Amex Checkout'
					),
					...generateObjectSchema(
						'amazonPay',
						{
							...generateTextFieldFrom([
								'type',
								'color',
								'size',
								'merchantId',
							]),
							...generateSettingsBoolFields([
								'singleCard'
							])
						},
						'Amazon Pay'
					),
					...generateObjectSchema(
						'googlePay',
						{
							...generateTextFieldFrom([
								'merchantCheckoutID',
								'env',
								'publicKey',
								'merchantId',
								'merchantName',
								'gateway',
							]),
							supportedCardTypes: {
								title: 'Supported Card Types',
								type: 'array',
								items: {
									title: 'Supported Card Type',
									items: {
										title: 'Card Type'
									}
								}
							}
						},
						'Google Pay'
					),
					...generateObjectSchema(
						'paypal',
						{
							...generateTextFieldFrom(['env'])
						},
						'PayPal'
					),
					...generateObjectSchema(
						'alipay',
						{
							...generateSettingsBoolFields(['instant']),
							...generateTextFieldFrom(['merchant_id']),
						},
						'Aliexpress Pay'
					),
				},
				'Alternative Wallets Settings'
			),
			...generateObjectSchema(
				'assets',
				{
					...generateObjectSchema(
						'paymentMethods',
						{
							...generateObjectSchema(
								'addCard',
								{
									...generateTextFieldFrom([
										'visa',
										'mastercard',
										'amex',
										'diners',
									])
								},
								'Add Card'
							),
							...generateObjectSchema(
								'manageCard',
								{
									...generateTextFieldFrom([
										'visa',
										'mastercard',
										'amex',
										'diners',
										'giftCard',
									])
								},
								'Manage Card'
							),
							...generateObjectSchema(
								'acceptedCards',
								{
									...generateTextFieldFrom([
										'visa',
										'mastercard',
										'amex',
										'diners',
									])
								},
								'Accepted Cards'
							),
							...generateObjectSchema(
								'alternativePaymentMethods',
								{
									...generateTextFieldFrom([
										'payPal',
										'applePay',
										'masterpass',
										'googlePay',
										'visaCheckout',
										'weChat',
										'payConiq',
									])
								},
								'Alternative Payment Methods'
							),
							...generateTextFieldFrom(['temporaryCard']),
						},
						'Payment Methods',
					)
				},
				'Assets'
			),
			...generateObjectSchema(
				'iconFonts',
				{
					path: {
						type: 'array',
						items: {
							title: 'Icon Path',
							items: {
								type: 'string',
							}
						}
					},
					...generateObjectSchema(
						'relations',
						{
							...generateTextFieldFrom([
								'i',
								'back',
								'trash',
								'Privacy2',
								'addCard',
								'x',
								'card',
								'scanCard',
								'default',
								'forward',
								'Privacy',
								'dropDown',
							])
						},
						'Relations'
					)
				},
				'Icon Fonts'
			),
			...generateObjectSchema(
				'features',
				{
					...generateObjectSchema(
						'creditCardProvider',
						{
							...generateTextFieldFrom(['name']),
						},
						'Credit Card Provider'
					),
					alternativeWallet: {
						type: 'array',
						items: {
							title: 'Alternative Wallet Name',
							items: {
								type: 'string',
							}
						}
					},
					acceptedCards: {
						type: 'array',
						items: {
							title: 'Accepted Card Name',
							items: {
								type: 'string',
							}
						}
					},
					...generateObjectSchema(
						'giftCards',
						{
							...generateSettingsBoolFields(['inPaymentMethods'])
						},
						'Gift Cards',
					)
				},
				'Features',
			)
		}
	}
});

export const WalletConfigSchema: ConfigSchema = {
	type: "object",
	properties: {
		...generateStyleSchemaPart(),
		...generateSettingsSchemaPart(),
	}
}
