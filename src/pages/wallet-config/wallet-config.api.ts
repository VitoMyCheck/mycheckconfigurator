import { WalletConfigRepository, WalletConfigResponse, WalletConfigResponseJSON } from './wallet-config.types';
import { DEFAULT_WALLET_VERSION } from './wallet-config';
import { ApiClient } from '@/infrastructure/api-client';

export class HttpWalletConfigRepository implements WalletConfigRepository {
	async fetchWalletConfig(domain: string, pkey: string, walletVersion: string = DEFAULT_WALLET_VERSION): Promise<WalletConfigResponse> {
		try {
			const walletVersionKey = walletVersion === DEFAULT_WALLET_VERSION ? 'walletConfiguration&publishableKey=' : 'walletV3&publishableKey=';
			const URL: string = `${domain}/users/api/v1/business/configurations?public_only=1&keys=${walletVersionKey}${pkey}`;
			const walletConfigData: WalletConfigResponseJSON = await ApiClient<WalletConfigResponseJSON>(URL);
			return walletConfigData.business.config;
		} catch (err) {
			throw err;
		}
	}
}
