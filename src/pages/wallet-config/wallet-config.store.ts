import { observable, action, makeObservable, computed } from 'mobx';
import { injector } from '@/injector';
import {
	DEFAULT_WALLET_VERSION,
	WALLET_CONFIG_SERVICE,
	WalletConfigService,
} from './wallet-config';
import {
	WalletConfigResponse,
	WalletEnvData,
	WalletEnvironmentListItem,
} from './wallet-config.types';

const DEFAULT_ENV: WalletEnvironmentListItem = {
	apiDomain: 'https://the-test.mycheckapp.com',
	env: 'test',
	walletVersion: DEFAULT_WALLET_VERSION,
}

class WalletConfigStore {
	private _walletService: WalletConfigService = injector.get<WalletConfigService>(WALLET_CONFIG_SERVICE);
	@observable
	private _walletConfig: WalletConfigResponse | undefined;
	@observable
	public editedWalletConfig: WalletConfigResponse | undefined = {};
	@observable
	public envData: WalletEnvData = {
		apiDomain: DEFAULT_ENV.apiDomain,
		pkey: '',
		env: DEFAULT_ENV.env,
		walletVersion: DEFAULT_ENV.walletVersion!,
	}
	
	constructor() {
		makeObservable<WalletConfigStore>(this);
	}
	
	@computed
	public get walletConfig() {
		if (!this._walletConfig) {
			return {};
		}

		if (this.envData.walletVersion === DEFAULT_WALLET_VERSION) {
			return this._walletConfig!.walletConfiguration;
		}

		return this._walletConfig!.walletV3;
	}
	
	public set walletConfig(config: WalletConfigResponse | undefined) {
		this._walletConfig = {...config};
	}
	
	@action
	public fetchWalletConfig = async (apiDomain: string, pkey: string, walletVersion: string): Promise<void> => {
		try {
			this.walletConfig = await this._walletService.fetchWalletConfig(apiDomain, pkey, walletVersion);
			this.editedWalletConfig = {...this.walletConfig};
		} catch (err) {
			console.log(err);
			this._walletConfig = undefined;
		}
	}
	
	@action
	public setEditedWalletConfig = (isValid: boolean, editedConfig: WalletConfigResponse): void => {
		this.editedWalletConfig = {...editedConfig};
		// TODO: Validation is more harmful for now
		// if (isValid) {
		// 	this.editedWalletConfig = {...editedConfig};
		// } else {
		// 	console.log('FORM NOT VALID YET');
		// }
	}
	
	@action
	public resetEditedWalletConfig = (): void => {
		this.editedWalletConfig = this.walletConfig;
	}
	
	@action
	public updateEnvData = (fieldName: string, value: string): void => {
		this.envData = { ...this.envData, [fieldName]: value };
	}
}

export const walletConfigStore: WalletConfigStore = new WalletConfigStore();
