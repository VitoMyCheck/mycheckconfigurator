import  { injector } from '@/injector';
import { WalletConfigRepository, WalletConfigResponse, WalletVersion } from './wallet-config.types';

export const WALLET_CONFIG_REPOSITORY: string = 'WALLET_CONFIG_REPOSITORY';
export const DEFAULT_WALLET_VERSION: WalletVersion = 'v2';

export const WALLET_CONFIG_SERVICE: string = 'WALLET_CONFIG_SERVICE';
export class WalletConfigService {
	private _walletConfigRepo = injector.get<WalletConfigRepository>(WALLET_CONFIG_REPOSITORY);

	public fetchWalletConfig(domain: string, pkey: string, walletVersion: string): Promise<WalletConfigResponse> {
		return this._walletConfigRepo.fetchWalletConfig(domain, pkey, walletVersion);
	}
}
