import {css, html, TemplateResult} from 'lit-element';
import {MobxLitElement} from '@adobe/lit-mobx';
import {action, observable} from 'mobx';
import '@polymer/paper-input/paper-input';
import '@polymer/paper-button/paper-button';
import '@polymer/paper-card/paper-card';
import '@polymer/paper-radio-group';
import '@polymer/paper-radio-button';
import '@polymer/paper-tabs/paper-tabs';
import '@polymer/paper-tabs/paper-tab';
import 'json-form-custom-element';

import './components';

const WALLET_ENV_TAB: string = 'wallet-env';
const WALLET_JSON_EDITOR: string = 'wallet-json-editor';

class WalletConfigPage extends MobxLitElement {
	@observable
	private _selectedTab: string = WALLET_ENV_TAB;
	
	static get styles() {
		return css`
			.paper-card {
				width: 100%;
				background: none;
				box-shadow: none;
			}
			.container {
				margin-bottom: 20px;
			}
			paper-tabs {
				background: white;
				color: black;
				--paper-tab-ink: grey;
			}
			paper-tabs paper-tab.iron-selected {
      	color: #3f51b5;
    	}
    	.divider {
    	  height: 85%;
        margin: auto 0;
        width: 1px;
        background: rgba(31, 38, 135, 0.17);
    	}
    	glass-container {
    	  --border-radius: 5px;
    	}
		`;
	}
	
	@action
	private _onTabChange = async (ev: any) => {
		this._selectedTab = ev.target.title;
		await this.requestUpdate();
	}
	
	render(): TemplateResult {
		return html`
			<paper-card class="paper-card container">
				<glass-container>
					<paper-tabs selected="0" autoselect no-bar noink>
						<paper-tab
							@click="${this._onTabChange}"
							title="${WALLET_ENV_TAB}"
						>Env Editor
						</paper-tab>
						<div class="divider"></div>
						<paper-tab
							@click="${this._onTabChange}"
							title="${WALLET_JSON_EDITOR}">JSON Editor
						</paper-tab>
					</paper-tabs>
			</paper-card>
			</glass-container>
			<div class="container">
				${this._selectedTab === WALLET_ENV_TAB
					? html`
						<wallet-env-configuration></wallet-env-configuration>`
					: html`
						<json-config-editor></json-config-editor>`
				}
			</div>
		`
	}
}

customElements.define('wallet-config-page', WalletConfigPage);
