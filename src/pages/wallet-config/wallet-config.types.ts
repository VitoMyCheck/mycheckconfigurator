export interface WalletConfigRepository {
	fetchWalletConfig(domain: string, pkey: string, walletVersion: string): Promise<any>;
}

export interface WalletConfigResponseJSON {
	business: {
		config: WalletConfigResponse,
	}
}

export interface WalletConfigResponse {
	walletConfiguration?: {
		[x: string]: any,
	},
	walletV3?: {
		[x: string]: any,
	}
}

export type WalletEnvironmentsNames = 'test' | 'sandbox' | 'production';
export type WalletVersion = 'v2' | 'v3';
export type WalletEnvironmentsURLs = 'https://the-test.mycheckapp.com'
	| 'https://the-sandbox.mycheckapp.com'
	| 'https://the.mycheckapp.com';

export interface WalletEnvData {
	apiDomain: WalletEnvironmentsURLs,
	env: WalletEnvironmentsNames,
	walletVersion: WalletVersion,
	pkey: string,
}

export interface WalletEnvironmentListItem {
	env: WalletEnvironmentsNames,
	apiDomain: WalletEnvironmentsURLs,
	walletVersion?: WalletVersion,
}
