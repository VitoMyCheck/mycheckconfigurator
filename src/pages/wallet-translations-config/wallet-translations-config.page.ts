import { html, css } from 'lit-element';
import { MobxLitElement } from '@adobe/lit-mobx';
import '@polymer/paper-input/paper-input';
import '@polymer/paper-button/paper-button';
import '@polymer/paper-card/paper-card';
import '@components';

class WalletTranslationsConfigPage extends MobxLitElement {
	static get styles() {
		return css`
			.paper-card {
				width: 100%;
				background: none;
				box-shadow: none;
			}
      glass-container {
    	  --border-radius: 5px;
    	}
		`;
	}

	render() {
		return html`
			<glass-container>
				<paper-card class="paper-card" heading='JSON configurator'>
					<div class="card-content">
						<paper-input always-float-label label="Floating label"></paper-input>
						<json-editor
							.onSendJSON=${() => {}}
							schema="{}"
							jsonData="{}"
						></json-editor>
					</div>
					<div class="card-actions">
						<paper-button>Save</paper-button>
						<paper-button>Cancel</paper-button>
					</div>
				</paper-card>
			</glass-container>
		`
	}
}

customElements.define('wallet-translations-config-page', WalletTranslationsConfigPage);
