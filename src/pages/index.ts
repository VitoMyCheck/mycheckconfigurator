import './wallet-config/wallet-config.page';
import './wallet-translations-config/wallet-translations-config.page';
import './payby-config/paby-config.page';
import './payby-translations-config/payby-translations-config.page';
import './not-found/not-found.page';
