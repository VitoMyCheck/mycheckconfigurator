import { injector } from './injector';
import {
	WALLET_CONFIG_REPOSITORY, WALLET_CONFIG_SERVICE,
	WalletConfigService
} from './pages/wallet-config/wallet-config';
import { WalletConfigRepository } from './pages/wallet-config/wallet-config.types';
import { HttpWalletConfigRepository } from './pages/wallet-config/wallet-config.api';

import {
	PAYBY_CONFIG_REPOSITORY,
	PAYBY_CONFIG_SERVICE,
	PayByConfigService,
} from './pages/payby-config/payby-config';
import { PayByConfigRepository } from './pages/payby-config/payby-config.types';
import { HttpPayByConfigRepository } from './pages/payby-config/payby-config.api';

injector.set<WalletConfigRepository>(WALLET_CONFIG_REPOSITORY, new HttpWalletConfigRepository());
injector.set<WalletConfigService>(WALLET_CONFIG_SERVICE, new WalletConfigService());

injector.set<PayByConfigRepository>(PAYBY_CONFIG_REPOSITORY, new HttpPayByConfigRepository());
injector.set<PayByConfigService>(PAYBY_CONFIG_SERVICE, new PayByConfigService());
