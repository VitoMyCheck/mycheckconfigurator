import { ipcRenderer, webFrame } from 'electron';
import { html, render } from 'lit-html';

const setupTemplate = () => {
	// @ts-ignore
	document.body.style = 'margin: 0';
	render(initTemplate(), document.body);
	
	ipcRenderer.on('payByLoaded', (ev, data) => {
		const { identifier } = data;
		const payByURL = `https://payby-test.mycheckapp.com/?identifier=${identifier}`;
		render(initTemplate(payByURL), document.body);
	})
	
	ipcRenderer.on('message', (ev, data) => {
		const payByIframe = document.querySelector('iframe')
		payByIframe!.contentWindow!.postMessage(data, '*');
	})
}

const initTemplate = (url = "") => html`
	<iframe style="width: 100%; height: 100vh" src=${url} id="walletContent" frameborder="0"></iframe>
`

setupTemplate();
