import { html, render } from 'lit-html';
import './injectDependencies';
import { routerStore } from './infrastructure/routing/router.store';
import './pages';
import '@components';
import { ROOT_ID } from './shared/consts';

const initialTemplate = () => html`
	<app-navigation-wrapper>
		<div id="${ROOT_ID}"></div>
	</app-navigation-wrapper>
`;

const setupTemplate = () => {
	// @ts-ignore
	document.body.style = 'margin: 0';
	render(initialTemplate(), document.body);
	routerStore.setupRouter();
}

setupTemplate();
