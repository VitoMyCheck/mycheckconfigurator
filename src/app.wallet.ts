import { ipcRenderer, webFrame } from 'electron';
import { html, render } from 'lit-html';

const setupTemplate = () => {
	// @ts-ignore
	document.body.style = 'margin: 0';
	render(initTemplate(), document.body);

	ipcRenderer.on('walletLoaded', (ev, data) => {
		const { walletVersion } = data;
		const walletURL = walletVersion === 'v2'
			? 'https://wallet-sdk-test.mycheckapp.com/mycheck/index.html'
			: 'https://mycheck-wallet-v3-test.s3-eu-west-1.amazonaws.com/mycheck/index.html';
		render(initTemplate(walletURL), document.body);
	})

	ipcRenderer.on('message', (ev, data) => {
		const walletIframe = document.querySelector('iframe')
		walletIframe!.contentWindow!.postMessage(data, '*');
	})
}

const initTemplate = (url = "") => html`
	<iframe style="width: 100%; height: 100vh" src=${url} id="walletContent" frameborder="0"></iframe>
`

setupTemplate();
