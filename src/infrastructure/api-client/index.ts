const parseDataFromAPI = async (data: any) => {
	try {
		const apiData = await data.json();
		if (apiData && apiData.status === 'ERROR') {
			throw new Error(apiData.message);
		}
		return apiData.responseBody || apiData;
	} catch (err) {
		throw err;
	}
}

export const ApiClient = async <T>(url: string, options?: any): Promise<T> => {
	try {
		if (!options || options && !options.method) {
			const apiResponse = await fetch(url);
			return await parseDataFromAPI(apiResponse);
		}

		const apiResponse = await fetch(url, options);
		return await parseDataFromAPI(apiResponse);
	} catch (err) {
		throw err;
	}
}
