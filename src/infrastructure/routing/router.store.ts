import { action, makeObservable, observable } from 'mobx';
import { Router } from '@vaadin/router';

import { ROOT_ID } from '@/shared/consts';

export interface IRoute {
	routeName: RouteName;
	path: string;
	component: string;
}

export type RouteName = 'Wallet Configurator'
	| 'Wallet Translation Configurator'
	| 'PayBy Configurator'
	| 'PayBy Translation Configurator'
	| '404';

export class RouterStore {
	@observable
	private _router: Router | undefined;
	@observable
	public routeName = '';

	private _routes: IRoute[] = [
		{ routeName: 'Wallet Configurator', path: '/', component: 'wallet-config-page' },
		// { routeName: 'Wallet Translation Configurator', path: '/wallet/translations', component: 'wallet-translations-config-page' },
		{ routeName: 'PayBy Configurator', path: '/payby/config', component: 'payby-config-page' },
		// { routeName: 'PayBy Translation Configurator', path: '/payby/translations', component: 'payby-translations-config-page' },
		{ routeName: '404', path: '(.*)', component: 'wallet-config-page' },
	];

	public get routes() {
		return this._routes.filter((route: IRoute) => route.routeName !== '404');
	}

	constructor() {
		this.routeName = this._routes[0].routeName;
		makeObservable<RouterStore>(this);
	}

	@action
	public routeChange = (routeName: RouteName, routePath: string = '/') => {
		this.routeName = routeName;
		if (this._router) {
			this._router.render(routePath);
		}
	}

	@action
	public setupRouter = (): void => {
		this._router = new Router(document.getElementById(ROOT_ID))
		this._router.setRoutes(this._routes.map((route: IRoute) => ({
			path: route.path,
			component: route.component,
		})));
	}
}

export const routerStore: RouterStore = new RouterStore();
