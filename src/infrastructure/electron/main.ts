import {app, BrowserView, BrowserWindow, dialog, ipcMain} from 'electron';
import * as fs from 'fs';
import * as path from 'path';

let mainWindow: Electron.BrowserWindow | null;
let walletWindow: Electron.BrowserWindow | null;
let payByWindow: Electron.BrowserWindow | null;
let walletVersion: string = 'v2';

const setupFileListener = () => {
	ipcMain.on('config', (event, data) => {
		dialog.showSaveDialog({
			title: 'Select the File Path to save',
			defaultPath: path.join(__dirname, '../assets/config.json'),
			// defaultPath: path.join(__dirname, '../assets/'),
			buttonLabel: 'Save',
			// Restricting the user to only Text Files.
			filters: [
				{
					name: 'Config',
					extensions: ['json']
				},
			],
			properties: []
		}).then(file => {
			// Stating whether dialog operation was cancelled or not.
			console.log(file.canceled);
			if (!file.canceled) {
				console.log(file.filePath!.toString());

				// Creating and Writing to the sample.txt file
				fs.writeFile(file.filePath!.toString(),
					data, function (err) {
						if (err) throw err;
						console.log('Saved!');
					});
			}
		}).catch(err => {
			console.log(err)
		});
	});

	//WALLET:
	ipcMain.on('openWalletWindow', (event, data) => {
		if (walletWindow && walletVersion === data.walletVersion) {
			return;
		} else if (walletWindow && walletVersion !== data.walletVersion) {
			walletVersion = data.walletVersion;
			walletWindow.close();
		}

		walletWindow = new BrowserWindow({
			width: 800,
			height: 600,
			webPreferences: {
				nodeIntegration: true,
				enableRemoteModule: true,
				webviewTag: true,
			},
		});

		if (process.env.NODE_ENV === 'development') {
			walletWindow.loadURL(`http://localhost:4000/index.wallet.html`);
		} else {
			walletWindow.loadFile(path.join(__dirname, './renderer/index.wallet.html'));
		}

		walletWindow.setTitle('MyCheck Wallet');

		walletWindow.on('closed', () => {
			walletWindow = null;
		});
		setTimeout(() => {
			walletWindow?.webContents.send('walletLoaded', {
				walletVersion: data.walletVersion
			})
		}, 1000)
	})

	ipcMain.on('walletConfiguratorChannel', (event, stringifiedConfig) => {
		walletWindow?.webContents.send('message', {
			actionType: 'walletConfiguratorAction',
			walletConfiguration: JSON.parse(stringifiedConfig)
		})
	})
	
	//PAYBY
	ipcMain.on('openPayByWindow', (event, data) => {
		if (payByWindow) {
			return;
		}
		
		payByWindow = new BrowserWindow({
			width: 800,
			height: 600,
			webPreferences: {
				nodeIntegration: true,
				enableRemoteModule: true,
				webviewTag: true,
			},
		});
		
		if (process.env.NODE_ENV === 'development') {
			payByWindow.loadURL(`http://localhost:4000/index.payby.html`);
		} else {
			payByWindow.loadFile(path.join(__dirname, './renderer/index.payby.html'));
		}
		
		payByWindow.setTitle('MyCheck Wallet');
		
		payByWindow.on('closed', () => {
			payByWindow = null;
		});
		
		setTimeout(() => {
			payByWindow?.webContents.send('payByLoaded', {
				identifier: data.identifier
			})
		}, 1000)
	});
	
	ipcMain.on('payByConfiguratorChannel', (event, stringifiedConfig) => {
		payByWindow?.webContents.send('message', {
			actionType: 'payByConfiguratorAction',
			payByConfiguration: JSON.parse(stringifiedConfig)
		})
	});
}

function createWindow() {
	mainWindow = new BrowserWindow({
		width: 800,
		height: 600,
		frame: true,
		webPreferences: {
			nodeIntegration: true,
		},
	});

	if (process.env.NODE_ENV === 'development') {
		mainWindow.loadURL(`http://localhost:4000`);
	} else {
		mainWindow.loadFile(path.join(__dirname, './renderer/index.html'));
	}

	mainWindow.setTitle('MyCheck Config Editor');

	mainWindow.on('closed', () => {
		mainWindow = null;
	});

	setupFileListener();
}

app.on('ready', createWindow);
app.allowRendererProcessReuse = true;
