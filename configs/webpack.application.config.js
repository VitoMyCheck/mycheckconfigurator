const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  resolve: {
    extensions: ['.ts', '.js'],
    mainFields: ['main', 'module', 'browser'],
    aliasFields: ['browser'],
    alias: {
      "@": path.resolve(__dirname, '../src/'),
      "@components": path.resolve(__dirname, '../src/shared/components/')
    }
  },
  entry: {
    root: ['./src/app.ts'],
    wallet: ['./src/app.wallet.ts'],
    payBy: ['./src/app.payby.ts'],
  },
  target: 'electron-renderer',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(js|ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        exclude: /node_modules/,
        use: {
          loader: 'file-loader?name=assets/[name].[hash].[ext]'
        }
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  devServer: {
    contentBase: path.join(__dirname, '../dist/renderer'),
    historyApiFallback: true,
    compress: true,
    hot: true,
    port: 4000,
    publicPath: '/',
  },
  output: {
    path: path.resolve(__dirname, '../dist/renderer'),
    filename: 'js/[name].js',
    publicPath: './',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      filename: "index.html",
      chunks: ["root"]
    }),
    new HtmlWebpackPlugin({
      template: "./src/index.wallet.html",
      filename: "index.wallet.html",
      chunks: ["wallet"]
    }),
    new HtmlWebpackPlugin({
      template: "./src/index.payby.html",
      filename: "index.payby.html",
      chunks: ["payBy"]
    })
  ],
};
