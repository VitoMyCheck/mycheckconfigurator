const path = require('path');

module.exports = {
  resolve: {
    extensions: ['.ts', '.js'],
  },
  devtool: 'source-map',
  entry: './src/infrastructure/electron/main.ts',
  target: 'electron-main',
  module: {
    rules: [
      {
        test: /\.(js|ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        exclude: /node_modules/,
        use: {
          loader: 'file-loader?name=assets/[name].[hash].[ext]'
        }
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].js',
  },
};
